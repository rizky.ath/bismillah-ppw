$( function() {
	$("#accordion").accordion({
		collapsible: true, 
		active: false,
		heightStyle: "content"
	});

	$('.down').click(function(e){
	      e.stopPropagation();
	      var id = $(this).closest('.section').attr('id')
	      var idSec = $(this).closest('.section-content').attr('id')

	      if (id != 4) {
	        var idNext = parseInt(id)+1
	        var idSecNext = parseInt(idSec)+1

	        $("#" + id).insertAfter($("#" + idNext))
	        $("#" + idSec).insertAfter($("#" + idSecNext))
	        $("#" + id).attr('id', idNext)
	        $("#" + idSec).attr('id', idSecNext)
	        $("#" + idNext).attr('id', id)
	        $("#" + idSecNext).attr('id', idSec)
	      } 
      
    });

    $('.up').click(function(e){
      e.stopPropagation();
      var id = $(this).closest('.section').attr('id')
      var idSec = $(this).closest('.section-content').attr('id')

      if (id != 1) {
        var idPrev = parseInt(id)-1
        var idSecPrev = parseInt(idSec)-1

        $('#' + id).insertBefore("#" + idPrev)
        $('#' + idSec).insertBefore("#" + idSecPrev)
        $("#" + id).attr('id', 5)
        $('#' + idSec).attr('id', 5)
        $("#" + idPrev).attr('id', id)
        $('#' + idSecPrev).attr('id', idSec)
        $("#5").attr('id', idPrev)
        $("#5").attr('id', idSecPrev)


        console.log($("#accordion").html())
        console.log(id)
        console.log(idPrev)
      }
    });
	
} );