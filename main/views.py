from django.shortcuts import render, redirect
from .forms import MatkulForm
from django.http import HttpResponseRedirect
from .models import Matkul

def home(request):
    return render(request, 'main/story3-page1.html')

def qna(request):
	return render(request, 'main/story3-page2.html')

def oldweb(request):
	return render(request, 'main/story1.html')

def addmatkul(request):
	if request.method == 'POST' :
		form = MatkulForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/matkuls')


	form = MatkulForm()
	return render(request, 'main/story5-page1.html', { 'form': form })

def deleteMatkul(request, pk):
	matkul = Matkul.objects.get(id=pk)
	if request.method=='POST':
		matkul.delete()
		return redirect('/matkuls')

	response = {'item' : matkul}
	return render(request, "main/story5-delete.html", response)


def showmatkul(request):
	matkuls = Matkul.objects.all()
	response = {'matkuls' : matkuls}
	html = 'main/story5-page2.html'
	return render(request, html, response)



