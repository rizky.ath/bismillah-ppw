from django import forms
from .models import Matkul

class MatkulForm(forms.ModelForm):
	class Meta:
		model = Matkul
		fields = ['nama_matkul', 'dosen_pengajar', 'jumlah_sks', 'deskripsi', 'ruang_kelas']


	

