from django.db import models


class Matkul(models.Model):
	nama_matkul = models.CharField(max_length = 30)
	dosen_pengajar = models.CharField(max_length = 30)
	jumlah_sks = models.CharField(max_length = 2)
	deskripsi = models.TextField()
	ruang_kelas = models.CharField(max_length = 15)

	def __str__(self):
		return self.nama_matkul