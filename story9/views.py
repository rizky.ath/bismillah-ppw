from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib import messages

# Create your views here.
def index(request):
	return render(request, 'story9/index.html')

def signup(request):
	form = CreateUserForm()

	if request.method == "POST":
		form = CreateUserForm(request.POST)
		if form.is_valid():
			uname = form.cleaned_data['username']
			email = form.cleaned_data['email']
			pass1 = form.cleaned_data['password1']
			new_user = User.objects.create_user(uname, email=email, password=pass1, is_staff=True)
			new_user.save()
			return redirect('/story9/login/')

	response = {'form': form}
	return render(request, 'story9/signup.html', response)

def login(request):
	
	if request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']

		user = authenticate(request, username=username, password=password)
		if user is not None:
			auth_login(request, user)
			return redirect('/story9/')
		else:
			response = {'error_message' : 'username OR password incorrect'}
			return render(request, 'story9/login.html', response)


	response = {}
	return render(request, 'story9/login.html', response)


