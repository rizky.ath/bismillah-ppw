from django import forms
from .models import Kegiatan, Person

class KegiatanForm(forms.ModelForm):
	class Meta:
		model = Kegiatan
		fields = ['name']

class PersonForm(forms.ModelForm):
	class Meta:
		model = Person
		fields = ["name"]