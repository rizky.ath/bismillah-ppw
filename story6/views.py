from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import KegiatanForm, PersonForm
from .models import Kegiatan, Person

# Create your views here.
def index(request):
	kegiatans = Kegiatan.objects.all()
	response = {'kegiatans' : kegiatans}
	return render(request, 'story6/kegiatan.html', response)

def addKegiatan(request):
	if request.method =='POST':
		form = KegiatanForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/story6')

	form = KegiatanForm()
	return render(request, 'story6/addKegiatan.html', {'form' : form})

def addPeserta(request, pk):
	kegiatan = Kegiatan.objects.get(id=pk)
	if request.method=='POST':
		form = PersonForm(request.POST)
		if form.is_valid():
			data = form.cleaned_data
			form.save()
			pesertaBaru = Person.objects.get(name=data["name"])
			kegiatan.peserta.add(pesertaBaru)
			

			return redirect('/story6')

	form = PersonForm()
	response = {'form' : form}
	return render(request, "story6/addPeserta.html", response)