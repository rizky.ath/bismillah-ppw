from django.db import models

# Create your models here.
class Person(models.Model):
	name = models.CharField(max_length = 30)

	def __str__(self):
		return self.name

class Kegiatan(models.Model):
	name = models.CharField(max_length = 30)
	peserta = models.ManyToManyField(Person)

	def __str__(self):
		return self.name
